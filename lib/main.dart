import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var backgroundImage = Image.network('https://wallpaperaccess.com/full/689842.jpg');
  File _storedFile;

  Future<void> _takePicture() async {
    final imagePicker = ImagePicker();
    final imageFile = await imagePicker.getImage(
      source: ImageSource.gallery,
    );
    setState(() {
      _storedFile = File(imageFile.path);
    });
  }

  Future<void> _takePhoto() async {
    final imagePicker = ImagePicker();
    final imageFile = await imagePicker.getImage(
      source: ImageSource.camera,
    );
    setState(() {
      _storedFile = File(imageFile.path);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Stack(
          children: [
            Container(
              width: double.infinity,
              height: double.infinity,
              child: _storedFile != null
                  ? Image.file(
                      _storedFile,
                      fit: BoxFit.fill,
                    )
                  : Image(
                      image: NetworkImage('https://wallpaperaccess.com/full/2821638.jpg'),
                      fit: BoxFit.fill,
                    ),
            ),
            /*Image(, fit: BoxFit.fill,),*/
            Positioned(
              bottom: 0,
              right: 10,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: Icon(Icons.image_search),
                    onPressed: _takePicture,
                    color: Colors.white,
                  ),
                  IconButton(
                    icon: Icon(Icons.camera),
                    onPressed: _takePhoto,
                    color: Colors.white,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
